import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

// Create a preconfigured axios instance
const api = axios.create({
  withCredentials: false,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
})

// Used to cancel certain API requests
let once = null

const store = new Vuex.Store({

  state: {
    /** CATALOGUES **/
    catalogues: [],
    catalogue: {},
    catalogueDistributions: [],
    catalogueDistributionsSize: 0,
    catalogueViolations: [],
    catalogueViolationsSize: 0,
    /** METRICS **/
    metrics: {},
    isLoadingMetrics: false,
    currentlyLoadedMetrics: null
  },

  getters: {
    /** CATALOGUES **/
    getCatalogue (state) {
      return state.catalogue
    },
    getAllCatalogues (state) {
      return state.catalogues
    },
    getCatalogueDistributions (state) {
      return state.catalogueDistributions
    },
    getCatalogueDistributionsSize (state) {
      return state.catalogueDistributionsSize
    },
    getCatalogueViolations (state) {
      return state.catalogueViolations
    },
    getCatalogueViolationsSize (state) {
      return state.catalogueViolationsSize
    },
    /** METRICS **/
    metrics (state) {
      return state.metrics
    },
    isLoadingMetrics (state) {
      return state.isLoadingMetrics
    },
    getCurrentlyLoadedMetrics (state) {
      return state.currentlyLoadedMetrics
    }
  },

  mutations: {
    /** CATALOGUES **/
    SET_ALL_CATALOGUES (state, payload) {
      state.catalogues = payload
    },
    SET_CATALOGUE (state, payload) {
      let catalogue = payload.catalogues.filter((catalogue) => catalogue.info.id.includes(payload.id))[0]
      state.catalogue = catalogue
    },
    SET_CATALOGUE_DISTRIBUTIONS (state, payload) {
      state.catalogueDistributions = payload
    },
    SET_CATALOGUE_DISTRIBUTIONS_SIZE (state, payload) {
      state.catalogueDistributionsSize = payload
    },
    SET_CATALOGUE_VIOLATIONS (state, payload) {
      state.catalogueViolations = payload
    },
    SET_CATALOGUE_VIOLATIONS_SIZE (state, payload) {
      state.catalogueViolationsSize = payload
    },
    /** METRICS **/
    SET_LOADING_METRICS (state, payload) {
      state.isLoadingMetrics = payload
    },
    SET_METRICS (state, payload) {
      state.metrics = payload
    },
    SET_CURRENTLY_LOADED_METRICS (state, payload) {
      state.currentlyLoadedMetrics = payload
    }
  },

  actions: {
    /** CATALOGUES **/
    loadAllCatalogues ({ state, commit }) {
      return new Promise((resolve, reject) => {
        api
          .get(`${Vue.prototype.$env.ROOT_API}info/catalogues`)
          // eslint-disable-next-line
          .then(response => {
            commit('SET_ALL_CATALOGUES', response.data)
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })
    },
    loadCatalogue ({ state, commit }, id) {
      if (state.catalogues.length > 0) {
        commit('SET_CATALOGUE', { catalogues: state.catalogues, id })
      } else {
        return new Promise((resolve, reject) => {
          api
            .get(`${Vue.prototype.$env.ROOT_API}info/catalogues`)
            // eslint-disable-next-line
            .then(response => {
              commit('SET_ALL_CATALOGUES', response.data)
              commit('SET_CATALOGUE', { catalogues: response.data, id })
              resolve(response)
            })
            .catch(error => {
              console.log(error)
              reject(error)
            })
        })
      }
    },
    loadCatalogueDistributions ({ state, commit }, { id, currentPage }) {
      return new Promise((resolve, reject) => {
        api
          .get(`${Vue.prototype.$env.ROOT_API}metrics/catalogues/${id}/distributions/reachability`, {
            params: {
              limit: 10,
              offset: (parseInt(currentPage) - 1) * 10
            }
          })
          // eslint-disable-next-line
          .then(response => {
            commit('SET_CATALOGUE_DISTRIBUTIONS', response.data.result.results)
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })
    },
    loadCatalogueDistributionsSize ({ state, commit }, id) {
      return new Promise((resolve, reject) => {
        api
          .get(`${Vue.prototype.$env.ROOT_API}metrics/catalogues/${id}/distributions/reachability`, {
            params: {
              limit: 1
            }
          })
          // eslint-disable-next-line
          .then(response => {
            commit('SET_CATALOGUE_DISTRIBUTIONS_SIZE', response.data.result.count)
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })
    },
    loadCatalogueViolations ({ state, commit }, { id, currentPage }) {
      return new Promise((resolve, reject) => {
        api
          .get(`${Vue.prototype.$env.ROOT_API}metrics/catalogues/${id}/violations`, {
            params: {
              limit: 10,
              offset: (parseInt(currentPage) - 1) * 10
            }
          })
          .then(response => {
            commit('SET_CATALOGUE_VIOLATIONS', response.data.result.results)
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })
    },
    loadCatalogueViolationsSize ({ state, commit }, id) {
      return new Promise((resolve, reject) => {
        api
          .get(`${Vue.prototype.$env.ROOT_API}metrics/catalogues/${id}/violations`, {
            params: {
              limit: 1
            }
          })
          .then(response => {
            commit('SET_CATALOGUE_VIOLATIONS_SIZE', response.data.result.count)
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            reject(error)
          })
      })
    },
    /** METRICS **/
    fetchMetrics ({ context, state, commit }, { path, id }) {
      // Only do an API request when no one else is currently doing this request.
      // This prevents from unnecessarily making additional API requests.
      // Should not be used for loading catalogue metrics, as this leads to inconsistent data.

      // The complete payload is the basically a merged payload of all dimensions
      // and in addition a dimension called 'scoring' with indicator 'timeBasedScoring'
      let completePayload = { }
      if (!state.isLoadingMetrics || path !== state.currentlyLoadedMetrics) {
        if (once) {
          once.cancel('Request cancelled by user')
        }

        once = axios.CancelToken.source()
        commit('SET_CURRENTLY_LOADED_METRICS', path)
        commit('SET_LOADING_METRICS', true)
        commit('SET_METRICS', {})

        return api
          .get(`${Vue.prototype.$env.ROOT_API}metrics${path}`, {
            cancelToken: once.token
          })
          .then(response => {
            // Step 1: load all canonical dimensions
            completePayload = {
              scoring: { }, // Treat scoring as an additional dimension
              ...response.data // Canonical dimensions
            }

            // Commit immediate result so the app can work with partial data
            // in case step 2 fails.
            commit('SET_METRICS', Object.assign(completePayload, {}))
            commit('SET_LOADING_METRICS', false)

            let date = new Date(Date.now())
            let currentDate = `${date.getFullYear()}-${(date.getMonth() < 9 ? (`0${(date.getMonth() + 1)}`) : (date.getMonth() + 1))}-${(date.getDate() < 10 ? (`0${date.getDate()}`) : date.getDate())}`

            return api
              .get(`${Vue.prototype.$env.ROOT_API}score/${id}`, {
                params: {
                  startDate: Vue.prototype.$env.SCORING_START_DATE,
                  endDate: currentDate,
                  resolution: 'day'
                },
                cancelToken: once.token
              })
          })
          .then(response => {
            // Step 2: insert 'inofficial' scoring dimension.
            // Having scoring dimension is easier to manage later on.
            if (response.data.Result && response.data.Result.length) {
              completePayload.scoring = {
                timeBasedScoring: response.data.Result.map(el => {
                  let result = {}
                  result.name = Object.keys(el)[0]
                  result.percentage = el[Object.keys(el)[0]]
                  return result
                })
              }
              commit('SET_METRICS', Object.assign(completePayload, {}))
            }
          })
          .catch(error => {
            if (axios.isCancel(error)) {
              console.log(error)
            } else {
              console.error(error)
            }
          })
      }
    }
  },

  modules: {
    globalModule: {
      namespaced: true,
      actions: {
        fetchMetrics ({ dispatch }) {
          dispatch('fetchMetrics', { path: '/global', id: 'global' }, { root: true })
        }
      }
    },
    catalogueModule: {
      namespaced: true,
      actions: {
        fetchMetrics ({ dispatch }, id) {
          dispatch('fetchMetrics', { path: `/catalogues/${id}`, id }, { root: true })
        }
      }
    },
    countryModule: {
      namespaced: true,
      actions: {
        fetchMetrics ({ dispatch }, id) {
          dispatch('fetchMetrics', { path: `/countries/${id}`, id }, { root: true })
        }
      }
    }
  }
})

export default store
