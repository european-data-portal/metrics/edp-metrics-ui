import Vue from 'vue'
import Router from 'vue-router'
import vueSmoothScroll from 'vue2-smooth-scroll'

import Dashboard from '@/components/Dashboard'
import DimensionsDetailsModal from '@/components/dimensions-cards/DimensionsDetailsModal'

import Catalogues from '@/components/Catalogues'
import CatalogueDetail from '@/components/CatalogueDetail'
import CatalogueDetailDashboard from '@/components/CatalogueDetailDashboard'
import CatalogueDetailDistributions from '@/components/CatalogueDetailDistributions'
import CatalogueDetailViolations from '@/components/CatalogueDetailViolations'

import Methodology from '@/components/Methodology'

Vue.use(vueSmoothScroll)
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/mqa/',
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      children: [{
        path: 'dimensions',
        redirect: { name: 'Dashboard' }
      },
      {
        path: 'dimensions/:dimension',
        name: 'dimensions-details',
        component: DimensionsDetailsModal,
        props: {
          dimensionsType: 'dimensions-details'
        }
      }]
    },
    {
      path: '/methodology',
      name: 'Methodology',
      component: Methodology
    },
    {
      path: '/catalogues',
      name: 'Catalogues',
      component: Catalogues,
      props: true,
      children: [
        {
          path: ':id',
          name: 'CatalogueDetail',
          components: {
            catalogueDetail: CatalogueDetail
          },
          props: true,
          children: [
            {
              path: '',
              name: 'CatalogueDetailDashboard',
              components: {
                catalogueDetailNavigation: CatalogueDetailDashboard
              },
              children: [{
                path: 'dimensions',
                redirect: { name: 'CatalogueDetailDashboard' }
              },
              {
                path: 'dimensions/:dimension',
                name: 'catalogue-dimensions-details',
                component: DimensionsDetailsModal,
                props: {
                  dimensionsType: 'catalogue-dimensions-details'
                }
              }]
            },
            {
              path: 'distributions',
              name: 'CatalogueDetailDistributions',
              components: {
                catalogueDetailNavigation: CatalogueDetailDistributions
              }
            },
            {
              path: 'violations',
              name: 'CatalogueDetailViolations',
              components: {
                catalogueDetailNavigation: CatalogueDetailViolations
              }
            }
          ]
        }
      ]
    }
  ]
})
