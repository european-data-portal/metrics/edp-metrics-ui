// Attribute values of all dimensions to expand metrics data
// with additional/prior knowledge.

// Helper object to describe which colors the graphs get
// The graphs can't refer to SCSS variables so we describe them here manually
const chartbgColors = {
  // Define primary colors for each dimension
  accessibility: '#0971b8',
  findability: '#00aeef',
  interoperability: '#2b3582',
  reusability: '#f26833',
  contextuality: '#f99f1b',
  scoring: '#000000',
  // secondary colors every graph will get
  // i.e., non-primary or non-important colors
  secondary: [
    'rgb(242, 242, 242)',
    'rgb(159, 242, 223)'
  ]
}

const GRAPH_TYPE = {
  bar: 'bar-chart',
  doughnut: 'doughnut-chart',
  line: 'line-chart'
}

const REDUCE_FUNCTION = {
  max (values) {
    if (!values) return null
    return values.reduce((prev, current) => (prev.percentage > current.percentage) ? prev : current)
  }
}

const dimensionsAttributes = {
  scoring: {
    label: 'Scoring',
    className: 'scoring',
    description: 'The Time Based Scoring Chart.',
    chartStyle: {
      backgroundColors: [
        chartbgColors.scoring,
        ...chartbgColors.secondary
      ]
    },
    indicators: {
      timeBasedScoring: {
        label: '',
        graphType: GRAPH_TYPE.line
      }
    }
  },
  accessibility: {
    label: 'Accessibility',
    className: 'accessibility',
    description: 'Expedita eaque illo sint explicabo quibusdam.',
    chartStyle: {
      backgroundColors: [
        chartbgColors.accessibility,
        ...chartbgColors.secondary
      ]
    },
    indicators: {
      accessUrlStatusCodes: {
        label: '',
        displayName: true,
        primary: REDUCE_FUNCTION.max,
        graphType: GRAPH_TYPE.bar
      },
      downloadUrlAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      downloadUrlStatusCodes: {
        label: '',
        displayName: true,
        primary: REDUCE_FUNCTION.max,
        graphType: GRAPH_TYPE.bar
      }
    }
  },
  findability: {
    label: 'Findability',
    className: 'findability',
    description:
        'Minus molestias, sapiente quaerat voluptate suscipit expedita eaque illo sint explicabo quibusdam eligendi dolorem.',
    chartStyle: {
      backgroundColors: [
        chartbgColors.findability,
        ...chartbgColors.secondary
      ]
    },
    indicators: {
      temporalAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      spatialAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      keywordAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      categoryAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      }
    }
  },
  interoperability: {
    label: 'Interoperability',
    className: 'interoperability',
    description:
        'Lorem ipsum dolor sit, amet consectetur adipisicing elit.',
    chartStyle: {
      backgroundColors: [
        chartbgColors.interoperability,
        ...chartbgColors.secondary
      ]
    },
    indicators: {
      formatMediaTypeNonProprietary: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      formatMediaTypeAlignment: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      formatMediaTypeMachineReadable: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      dcatApCompliance: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      mediaTypeAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      formatAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      }
    }
  },
  reusability: {
    label: 'Reusability',
    className: 'reusability',
    description:
        'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minus molestias, sapiente.',
    chartStyle: {
      backgroundColors: [
        chartbgColors.reusability,
        ...chartbgColors.secondary
      ]
    },
    indicators: {
      contactPointAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      licenceAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      licenceAlignment: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      accessRightsAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      publisherAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      accessRightsAlignment: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      }
    }
  },
  contextuality: {
    label: 'Contextuality',
    className: 'contextual',
    description: 'Lorem ipsum dolor sit, amet.',
    chartStyle: {
      backgroundColors: [
        chartbgColors.contextuality,
        ...chartbgColors.secondary
      ]
    },
    indicators: {
      dateIssuedAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      dateModifiedAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      byteSizeAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      },
      rightsAvailability: {
        label: '',
        graphType: GRAPH_TYPE.doughnut
      }
    }
  }
}

const dimensionsAttributesOrdered = {
  scoring: dimensionsAttributes.scoring,
  findability: dimensionsAttributes.findability,
  accessibility: dimensionsAttributes.accessibility,
  interoperability: dimensionsAttributes.interoperability,
  reusability: dimensionsAttributes.reusability,
  contextuality: dimensionsAttributes.contextuality
}

export default dimensionsAttributesOrdered
